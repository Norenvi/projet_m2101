#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <stdlib.h>
#include <locale.h>
#include "VerifAlphanumAccents.h"
#include "vigenere.h"
#include "cesar.h"
#define SIZE 500


void main () {

//setlocal
struct lconv *loc;
setlocale (LC_ALL, "");
loc=localeconv();

//interface utilisateur de l'algorithme
wprintf(L"\n-------------------------\n");
wprintf(L"Algorithme de chiffrement\n");
wprintf(L"-------------------------\n");

wprintf(L"\n");
wchar_t texte[SIZE]={0};
wprintf(L"Veuillez entrer le message que vous souhaitez chiffrer :\n");
fgetws(texte,SIZE,stdin);

//déclaration des clés de chiffrement
wchar_t cleVig[20]={0};//Chiffrage Vigenère
wchar_t cleVig1[20]={0};//Déchiffrage Vigenère
int cleCesar;//Chiffrage Cesar
int cleCesar1;//Déchiffrage Cesar
wchar_t choix[2]={0};//Choix de l'algorithme
wchar_t choix2[2]={0};//Choix chiffrement/déchiffrement

//On vérifie d'abord que la chaîne de caractères entrée
//est bien alphanumérique
if(verifierAlphanumerique(texte)){
	convertirAccents(texte);
	wprintf(L"\nMessage initial : %ls\n", texte);
	//On propose à l'utilisateur de choisir parmi 
	//les deux algorithmes
		do {
		wprintf(L"\n");
		wprintf(L"Veuillez choisir un algorithme de chiffrement \n");
		wprintf(L"	1 - Chiffrement César\n");
		wprintf(L"	2 - Chiffrement Vigenère\n");
		wprintf(L"-----------------------\n");
		wprintf(L"Choix ( 1 ou 2 ) : ");
		wscanf(L"%ls",choix);
		if (choix[0]!=L'1' && choix[0]!=L'2') {
			wprintf(L"\nChoix invalide, veuillez choisir à nouveau (1 ou 2)\n\n");
		} 
	} while (choix[0]!=L'1' && choix[0]!=L'2');
		//Ensuite on demande à l'utilisateur de choisir entre le chiffrement et le déchiffrement	
	do {
		wprintf(L"\nChoisissez entre Chiffrer et Déchiffrer : \n");
		wprintf(L"	1 - Chiffrer\n");
		wprintf(L"	2 - Déchiffrer\n");
		wprintf(L"-----------------------\n");
		wprintf(L"Choix ( 1 ou 2 ) : ");
		wscanf(L"%ls",choix2);
		if (choix2[0]!=L'1' && choix2[0]!=L'2') {
			wprintf(L"\nVeuillez ressaisir votre choix\n");
		}
	} while (choix2[0]!=L'1' && choix2[0]!=L'2');
		//Résultats de l'algorithme César : 
		//Si l'utilisateur a choisi le chiffrement
	wprintf(L"\n");
			if(choix2[0]==L'1' && choix[0]==L'1')
			{
				wprintf(L"-----------------\n");
				wprintf(L"Chiffrement César\n");
				wprintf(L"-----------------\n\n");
				wprintf(L"Choisissez une clé de chiffrement (Nombre) : ");
				wscanf(L"%ld",&cleCesar);
				ChiffrerCesar(texte,cleCesar);
				wprintf(L"\nMessage crypté obtenu: %ls\n",texte);
			}
		//Si l'utilisateur a choisi le déchiffrement
	wprintf(L"\n");
		if(choix2[0]==L'2' && choix[0]==L'1')
			{
				wprintf(L"-------------------\n");
				wprintf(L"Déchiffrement César\n");
				wprintf(L"-------------------\n\n");
				wprintf(L"Choisissez une clé de déchiffrement (Nombre) : ");
				wscanf(L"%ld",&cleCesar1);
				DechiffrerCesar(texte,cleCesar1);
				wprintf(L"\nMessage décrypté obtenu: %ls\n",texte);
			}

	//Résultats de l'algorithme Vigenère : 

	//Si l'utilisateur a choisi le chiffrement
	wprintf(L"\n");
			if(choix2[0]==L'1' && choix[0]==L'2')
			{
				wprintf(L"--------------------\n");
				wprintf(L"Chiffrement Vigenère\n");
				wprintf(L"--------------------\n\n");
				wprintf(L"Choisissez une clé de chiffrement (Mot ou suite de lettres) : ");
				wscanf(L"%ls",&cleVig);
				ChiffrerVigenere(texte,cleVig);
				wprintf(L"\nMessage crypté obtenu: %ls\n",texte);
			}
		//Si l'utilisateur a choisi le déchiffrement
	wprintf(L"\n");
			if(choix2[0]==L'2' && choix[0]==L'2')
			{
				wprintf(L"----------------------\n");
				wprintf(L"Déchiffrement Vigenère\n");
				wprintf(L"----------------------\n\n");
				wprintf(L"Choisissez une clé de déchiffrement (Mot ou suite de lettres) : ");
				wscanf(L"%ls",&cleVig1);
				DechiffrerVigenere(texte,cleVig1);
				wprintf(L"\nMessage obtenu: %ls\n",texte);
			}

	wchar_t t[2]={0};
	wprintf(L"Souhaitez vous stocker le résultat du chiffrage dans un fichier texte ? (o/n) : ");
	wscanf(L"%ls",t);
			if (t[0]==L'o')
			{
				FILE* fichier = NULL;
				fichier = fopen("chiffrement.txt", "a");
				//affichage des différents chiffrage et clé utiliser dans le fichier
				//affichage de la clé dans le fichier
				if(choix[0]==L'1' && choix2[0]==L'1'){
					fwprintf(fichier,L"\n---------------\n");
					fwprintf(fichier,L"Chiffrement Cesar\n");
					fwprintf(fichier,L"---------------\n");
					fwprintf(fichier,L"Clé : %d\n",cleCesar);
				}else if(choix[0]==L'2' && choix2[0]==L'1'){
					fwprintf(fichier,L"\n-----------------\n");
					fwprintf(fichier,L"Déchiffrement Cesar\n");
					fwprintf(fichier,L"-----------------\n");
					fwprintf(fichier,L"Clé : %d\n",cleCesar1);
				}else if (choix[0]==L'1' && choix2[0]==L'2'){
					fwprintf(fichier,L"\n------------------\n");
					fwprintf(fichier,L"Chiffrement Vigenère\n");
					fwprintf(fichier,L"------------------\n");
					fwprintf(fichier,L"Clé : %ls\n",cleVig);
				}else{
					fwprintf(fichier,L"\n--------------------\n");
					fwprintf(fichier,L"Déchiffrement Vigenère\n");
					fwprintf(fichier,L"--------------------\n");
					fwprintf(fichier,L"Clé : %ls\n",cleVig1);
				}
				

				//affiche du message avant/après chiffrage dans le fichier	
				fwprintf(fichier,L"Le message initial : %ls",texte);
				if (choix[0]==L'1')
				{
					fwprintf(fichier, L"Le message codé : %ls", texte);
				}else{
					fwprintf(fichier, L"Le message décodé : %ls", texte);
				}
				fclose(fichier);
				wprintf(L"Le fichier chiffrement.txt a été créé\n");
				
			}

		}else{
			wprintf(L"Le message contient des caractères spéciaux, impossible de le chiffrer\n");
		}
}