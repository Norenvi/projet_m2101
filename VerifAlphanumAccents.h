#include <stddef.h>
//Si le message est alphanumérique, la fonction retourne 1, sinon elle retourne 0 et le programme s'arrête 
int verifierAlphanumerique(wchar_t*);
//S’il y a des caracetere accentués, on les remplace par des lettres non-accentuées
void convertirAccents(wchar_t*);