#include <stddef.h>
//Chiffre le texte par la méthode Vigenère et retourne le résultat
void ChiffrerVigenere (wchar_t *message,wchar_t *cleVig);
//Déchiffre le texte par la méthode Vigenère et retourne le résultat
void DechiffrerVigenere (wchar_t *message,wchar_t *cleVig1);