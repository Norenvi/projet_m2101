#include<stdio.h>
#include<string.h>
#include <stdlib.h>
#include <wchar.h>
#include "cesar.h"
#define SIZE 50

void ChiffrerCesar(wchar_t* texte,int n){

    while(n<0){
        n+=26;
    }
    for(int i = 0 ;i < wcslen(texte)-1;i++){
        //decalager n pour les caractere A-Z
        if(texte[i]>=L'A'&& texte[i]<=L'Z'){
            texte[i]=(texte[i]- 'A' + n)%26 + 'A';
        }else
         //decalager n pour les caractere a-z
        if(texte[i]>=L'a'&& texte[i]<=L'z'){
            texte[i]=(texte[i]- 'a' + n)%26 + 'a';
        }else
        //decalager n pour les caractere 0-9
        if(texte[i]>=L'0'&& texte[i]<=L'9'){
            texte[i]=(texte[i]- '0' + n)%10 + '0';
        }
    }
 

}

void DechiffrerCesar(wchar_t* texte,int n){

    for(int i = 0 ;i < wcslen(texte)-1;i++){
        //decalager n pour les caractere A-Z
        if(texte[i]>=L'A'&& texte[i]<=L'Z'){
            texte[i]=(texte[i]- 'A' + 26 - n)%26 + 'A';
        }else
         //decalager n pour les caractere a-z
        if(texte[i]>=L'a'&& texte[i]<=L'z'){
            texte[i]=(texte[i]- 'a' + 26 - n)%26 + 'a';
        }else
        //decalager n pour les caractere 0-9
        if(texte[i]>=L'0'&& texte[i]<=L'9'){
            texte[i]=(texte[i]- '0' + 26 - n)%10 + '0';
        }
    }
    
}