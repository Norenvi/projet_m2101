#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <string.h>

void ChiffrerVigenere (wchar_t *texte,wchar_t *cleVig){
    int i =0;
    int t=wcslen(texte);
    int cpt=0;
    wchar_t c;
    while(cpt<t)
    {
        c = *texte;
        if(c>='A' && c<='Z'){
            c=(c-'a'+cleVig[i]-'A')%26+'A';
            *texte=c;
            i=(i+1)%wcslen(cleVig);
        }
        if(c>='a' && c<='z'){
            c=(c-'a'+cleVig[i]-'a')%26+'a';
            *texte=c;
            i=(i+1)%wcslen(cleVig);
        }
        cpt++;
        texte++;
        
    }
}

void DechiffrerVigenere (wchar_t *texte,wchar_t *cleVig1){
    int t=wcslen(texte);
    wchar_t c;
    int cpt=0;
    int i =0;
    while(cpt<t)
    {
        c = *texte;
        if(c>='A' && c<='Z'){
            c=(c-'A'-cleVig1[i]+'a'+26)%26+'A';
            *texte=c;
            i=(i+1)%wcslen(cleVig1);
        }
        if(c>='a' && c<='z'){
            c=(c-'a'-cleVig1[i]+'a'+26)%26+'a';
            *texte=c;
            i=(i+1)%wcslen(cleVig1);
        }
        cpt++;
        texte++;
    }
}
