#include<stdio.h>
#include<string.h>
#include <stdlib.h>
#include <ctype.h>
#include <wctype.h>
#include <wchar.h>
#include "VerifALphanumAccents.h"
#define SIZE 50
    
int verifierAlphanumerique(wchar_t* texte){
	int a;
	a=0;
	for (int i = 0; i < wcslen(texte)-1; ++i){
		if (iswalnum(texte[i])){
			a=1;
		}else{
			a=0;
		}
	}
	return a;
}


void convertirAccents(wchar_t* texte){
     for(int i = 0 ;i < wcslen(texte) ; i++){
        //Si il y a des caractere hors a-z et 0-9
        if(texte[i]<'a'||texte[i]>'z'||texte[i]<'A'||texte[i]>'Z'||
           texte[i]<'0'||texte[i]>'9'){
           switch(texte[i]){
                //pour les caractere accentues, on remplace le caractere
                case L'À':case L'Á':case L'Â':case L'Ã':case L'Ä':
                texte[i]='A';
                break;
                case L'È':case L'É':case L'Ê':case L'Ë':
                texte[i]='E';
                break;
                case L'Ç' :
                texte[i]='C';
                break;
                case L'Ì' : case L'Í' : case L'Î' : case L'Ï':
                texte[i]='I';
                break;
                case L'Ò' : case L'Ó' : case L'Ô' : case L'Õ' : case L'Ö':
                texte[i]='O';
                break;
                case L'Ù' : case L'Ú' : case L'Û' : case L'Ü':
                texte[i]='U';
                break;
                case L'Ý' :
				texte[i]='Y';
				break;
				case L'Ñ' :
				texte[i]='N';
				break;
				case L'à' : case L'á' : case L'â' : case L'ã' : case L'ä':
				texte[i]='a';
				break;
				case L'è' : case L'é' : case L'ê' : case L'ë' :
				texte[i]='e';
				break;
				case L'ì' : case L'í' : case L'î' : case L'ï' :
				texte[i]='i';
				break;
				case L'ñ' :
				texte[i]='n';
				break;
				case L'ç' :
				texte[i]='c';
				break;
				case L'ò' : case L'ó' : case L'ô' : case L'õ' : case L'ö' :
				texte[i]='o';
				break;
				case L'ù' : case L'ú' : case L'û' : case L'ü':
				texte[i]='u';
				break;
				case L'ý' : case L'ÿ' :
				texte[i]='y';
				break;
				//pour le reste caracetere special, on raise exception Caractere_Invalide
				default:
                break;
           }
        }
    }
}