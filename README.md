# PROJET_M2101 - Sujet 4 - Chiffrement

# Présentation
Le but de ce programme est de chiffrer un message textuel (sans caractères spéciaux) en suivant le principe de deux algorithmes de chiffrement :
l'algorithme de César et l'algorithme de Vigenère

# Utilisation
Afin de compiler le programme pour pouvoir l'utiliser, utilisez la commande `make all` qui compilera chaque paquetage en une fois.
Ensuite, exécutez le programme à l'aide de la commande `./main`

Attention, le message que vous entrerez :
* Ne doit contenir que des lettres et/ou des caractères accentués
* Peut contenir des espaces
* Doit faire moins de 500 caractères

# Ressources
* [Chiffrement de César](https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage)
* [Chiffrement de Vigenère](https://fr.wikipedia.org/wiki/Chiffre_de_Vigen%C3%A8re)

# Signature des fonctions
```c
//Chiffre le texte par la méthode Cesar, par le décalage n et retourne le résultat
void ChiffrerCesar(wchar_t*,int n);
//Chiffre le texte par la méthode Cesar, par le décalage n et retourne le résultat
void DechiffrerCesar(wchar_t*,int n);

//Chiffre le texte par la méthode Vigenère et retourne le résultat
void ChiffrerVigenere (wchar_t *message,wchar_t *cleVig);
//Déchiffre le texte par la méthode Vigenère et retourne le résultat
void DechiffrerVigenere (wchar_t *message,wchar_t *cleVig1);

//Si le message est alphanumérique, la fonction retourne 1, sinon elle retourne 0 et le programme s'arrête 
int verifierAlphanumerique(wchar_t*);
//S’il y a des caracetere accentués, on les remplace par des lettres non-accentuées
void convertirAccents(wchar_t*);
```

# Développeurs

**MENG Fei**
* Chiffrement César
* Déchiffrement César
* Vérification Alphanumérique
* Application

**LAURENS VIGNERON Antoine**
* Chiffrement Vigenère
* Déchiffrement Vigenère
* Conversion des accents
* Application