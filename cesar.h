#include <stddef.h>

//Chiffre le texte par la méthode Cesar, par le décalage n et retourne le résultat
void ChiffrerCesar(wchar_t*,int n);
//Chiffre le texte par la méthode Cesar, par le décalage n et retourne le résultat
void DechiffrerCesar(wchar_t*,int n);